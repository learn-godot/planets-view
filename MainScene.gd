extends Spatial

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# load json
	var err = 0
	var http = HTTPClient.new() # Create the Client
		
	err = http.connect_to_host("ed-go.ru",80) # Connect to host/port
	assert(err == OK) # Make sure connection was OK
		
	# Wait until resolved and connected
	while http.get_status() == HTTPClient.STATUS_CONNECTING or http.get_status() == HTTPClient.STATUS_RESOLVING:
		http.poll()
		print("Connecting..")
		OS.delay_msec(500)
		
	assert(http.get_status() == HTTPClient.STATUS_CONNECTED) # Could not connect
		
	# Some headers
	var headers = [
		"User-Agent: Pirulo/1.0 (Godot)",
		"Accept: */*"
	]
		
	err = http.request(HTTPClient.METHOD_GET, "/ajax-test/systems/?startdate=2018-08-28", headers) # Request a page from the site (this one was chunked..)
	assert(err == OK) # Make sure all is OK
		
	while http.get_status() == HTTPClient.STATUS_REQUESTING:
		# Keep polling until the request is going on
		http.poll()
		print("Requesting..")
		OS.delay_msec(500)
		
	assert(http.get_status() == HTTPClient.STATUS_BODY or http.get_status() == HTTPClient.STATUS_CONNECTED) # Make sure request finished well.
		
	print("response? ", http.has_response()) # Site might not have a response.
		
	if http.has_response():
		# If there is a response..
		
		headers = http.get_response_headers_as_dictionary() # Get response headers
		print("code: ", http.get_response_code()) # Show response code
		print("**headers:\\n", headers) # Show headers
		
		# Getting the HTTP Body
		
		if http.is_response_chunked():
			# Does it use chunks?
			print("Response is Chunked!")
		else:
			# Or just plain Content-Length
			var bl = http.get_response_body_length()
			print("Response Length: ",bl)
		
		# This method works for both anyway
		
		var input = http.get("connection");
		print("Has bytes : ", input.get_available_bytes());
		
		
		var recievText = "";
		var recFlag = true;
		
		while recFlag:
			var buffer = input.get_utf8_string(input.get_available_bytes())
			print("buffer got: ", buffer.length())
			if buffer.length() == 0:
				recFlag = false
			else:
				recievText = recievText + buffer;
		
		var systems = JSON.parse(recievText);
		print("Json: ", systems.result.maxdate)
		
	# Done!
		
		#print("bytes got: ", recievText.length())
		#print("Text: ", recievText)
		
	#quit()

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass
